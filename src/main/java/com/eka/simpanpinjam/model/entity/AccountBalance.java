/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.model.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * Account Balance Entity Model
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "account_balance")
public class AccountBalance extends BaseEntity {
    /* Constants: */

    /**
     * 
     */
    private static final long serialVersionUID = -986898096298386556L;

    /* Attributes: */
    /**
     * Account Balance Id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    /**
     * Account balance
     */
    @Column(name = "balance")
    private BigDecimal balance;
    
    /**
     * Version data. Needed by jpa for locking.
     */
    @Version
    @Column(name = "version")
    private Integer version;
    
    /**
     * is master balance
     */
    @Column(name = "master_balance", columnDefinition="varchar(1) default 'N'")
    @Type(type = "yes_no")
    private boolean masterBalance=false;
    
    /**
     * Member who has the balance
     */
    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "member", referencedColumnName = "id")
    private AppUser member;

    /* Transient Attributes: */

    /* Constructors: */

    /* Getters & setters for attributes: */

    /* Getters & setters for transient attributes: */

    /* Functionalities: */

    /* Overrides: */
}
