/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.web.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.eka.simpanpinjam.model.common.CommonResponse;

/**
 * Exception Handler
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@ControllerAdvice
public class RestExceptionHandler  extends ResponseEntityExceptionHandler  {
    /* Constants: */
    private final Logger log = LoggerFactory.getLogger(RestExceptionHandler.class);
    private static final String FIELD_SEPARATOR = "\\.";

    /* Attributes: */

    /* Functionalities: */
    @ExceptionHandler({ ValidationException.class })
    protected ResponseEntity<CommonResponse> handleNotAccept(final RuntimeException ex, final WebRequest request) {
	log.error("not accept, caused by: "+ ex.getCause(), ex);
	
	CommonResponse response = new CommonResponse();
	response.setSuccess(false);
	response.setMessage(ex.getMessage());
	
	return new ResponseEntity<CommonResponse>(response, HttpStatus.NOT_ACCEPTABLE);
    }
    
    @ExceptionHandler(DataNotFoundException.class)
    public ResponseEntity<CommonResponse> handleNotFound(final RuntimeException ex, final WebRequest request) {
	log.error("handle not found: " + ex.getMessage(), ex);

	CommonResponse response = new CommonResponse();
	response.setSuccess(false);
	response.setMessage(ex.getMessage());

	return new ResponseEntity<CommonResponse>(response, HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler(Exception.class)
    public ResponseEntity<CommonResponse> handleInternal(Exception ex, 
                HttpServletRequest request, HttpServletResponse resp) {
	log.error("handle internal: "+ ex.getMessage(), ex);
	
	CommonResponse response = new CommonResponse();
	response.setSuccess(false);
	response.setMessage("Ooops.. something went wrong. Please try again!");
	
	return new ResponseEntity<CommonResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    

    /* Overrides: */
    /* (non-Javadoc)
     * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler#handleMethodArgumentNotValid(org.springframework.web.bind.MethodArgumentNotValidException, org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus, org.springframework.web.context.request.WebRequest)
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
	    HttpHeaders headers, HttpStatus status, WebRequest request) {
	log.error("handle method argument not valid", ex);
	BindingResult br = ex.getBindingResult();
	
	// get error field name
	String[] fields = br.getFieldError().getField().split(FIELD_SEPARATOR);
	String field = fields[0];
	
	String message = br.getFieldError().getDefaultMessage()+": "+field;
	CommonResponse response = new CommonResponse();
	response.setSuccess(false);
	response.setMessage(message);
	
	return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
    }

    /* (non-Javadoc)
     * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler#handleHttpMessageNotReadable(org.springframework.http.converter.HttpMessageNotReadableException, org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus, org.springframework.web.context.request.WebRequest)
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
	    HttpHeaders headers, HttpStatus status, WebRequest request) {
	log.error("override message not readable handler: "+ ex.getMessage(), ex);
	
	CommonResponse response = new CommonResponse();
	response.setSuccess(false);
	response.setMessage("Invalid request body");
	
	return new ResponseEntity<>(response, headers, status);
    }

    /* (non-Javadoc)
     * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler#handleExceptionInternal(java.lang.Exception, java.lang.Object, org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus, org.springframework.web.context.request.WebRequest)
     */
    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
	    HttpStatus status, WebRequest request) {
	log.error("override internal handler: "+ ex.getCause(), ex);
	
	CommonResponse response = new CommonResponse();
	response.setSuccess(false);
	response.setMessage("Ooops.. something went wrong. Please try again!");
	
	return new ResponseEntity<>(response, headers, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    
    
}
