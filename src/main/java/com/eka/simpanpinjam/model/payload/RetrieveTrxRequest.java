/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.model.payload;

import java.io.Serializable;
import java.time.LocalDate;

import com.eka.simpanpinjam.model.common.SimpanPinjamConstants;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * Retrieve Transaction Request
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@Data
public class RetrieveTrxRequest implements Serializable {

    /* Constants: */
    /**
     * 
     */
    private static final long serialVersionUID = 6099966384092002117L;

    /* Attributes: */
    /**
     * Start Date in yyyy-MM-dd format
     */
    @JsonFormat(pattern = SimpanPinjamConstants.DATE_FORMAT)
    private LocalDate startDate;
    
    /**
     * End Date in yyyy-MM-dd format
     */
    @JsonFormat(pattern = SimpanPinjamConstants.DATE_FORMAT)
    private LocalDate endDate;
    
    /**
     * Member Id
     */
    private Long memberId;
    

    /* Constructors: */


    /* Functionalities: */

}
