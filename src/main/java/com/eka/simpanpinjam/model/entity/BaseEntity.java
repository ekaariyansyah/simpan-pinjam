/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.eka.simpanpinjam.model.common.SimpanPinjamConstants;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Base Entity Model
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {


    @CreatedBy
    @Column(name = "created_by")
    private String createdBy;
    
    @JsonFormat(pattern = SimpanPinjamConstants.DATETIME_FORMAT)
    @CreatedDate
    @Column(name = "created_date_time")
    private LocalDateTime createdDateTime;
    
    @LastModifiedBy
    @Column(name = "modified_by")
    private String modifiedBy;
    
    @JsonFormat(pattern = SimpanPinjamConstants.DATETIME_FORMAT)
    @LastModifiedDate
    @Column(name = "modified_date_time")
    private LocalDateTime modifiedDateTime;

}
