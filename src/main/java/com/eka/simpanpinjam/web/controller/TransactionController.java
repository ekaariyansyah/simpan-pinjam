/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.eka.simpanpinjam.core.service.TransactionService;
import com.eka.simpanpinjam.model.common.CommonRequest;
import com.eka.simpanpinjam.model.common.CommonResponse;
import com.eka.simpanpinjam.model.entity.Transaction;
import com.eka.simpanpinjam.model.payload.CreateTrxRequest;
import com.eka.simpanpinjam.model.payload.RetrieveTrxRequest;

/**
 * REST Controller to manage Transaction
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@CrossOrigin
@RequestMapping("/transaction")
@RestController
public class TransactionController {
    /* Constants: */
    

    /* Services: */
    @Autowired
    private TransactionService trxService;
    

    /* Functionalities: */
    /**
     * Create Transaction
     * @param request
     * @return
     */
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CommonResponse createTransaction(@RequestBody @Valid CreateTrxRequest request) {
	Transaction trx = trxService.createTrx(request);
	
	return CommonResponse.createSuccessWithData(trx);
    }
    
    /**
     * Retrieve Transaction
     * @param request
     * @return
     */
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse retrieveTransaction(@RequestBody @Valid CommonRequest<RetrieveTrxRequest> request) {
	CommonResponse response = trxService.retrieveTrx(request);
	
	return response;
    }
    
    /**
     * Get Transactions of Member
     * @param memberid Member Id to find
     * @return
     */
    @GetMapping("/member/{memberId}")
    public CommonResponse getMemberTransaction(@PathVariable("memberId") Long memberid) {
	List<Transaction> trx = trxService.getTrxMember(memberid);
	
	return CommonResponse.createSuccessWithData(trx);
    }
    

    /* Overrides: */
}
