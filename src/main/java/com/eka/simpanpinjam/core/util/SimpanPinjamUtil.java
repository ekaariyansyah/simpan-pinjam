/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.core.util;

//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Application Utilities
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
public class SimpanPinjamUtil {
    /* Constants: */

    /* Attributes: */

    /* Transient Attributes: */

    /* Constructors: */

    /* Getters & setters for attributes: */

    /* Getters & setters for transient attributes: */

    /* Functionalities: */
//    /**
//     * Encodes BCrypt password.
//     * 
//     * @param password The password to encode.
//     * @return Encoding result.
//     */
//    public static String encodeBCryptPassword(String password) {
//        BCryptPasswordEncoder encoderBCrypt = new BCryptPasswordEncoder();
//
//        return encoderBCrypt.encode(password);
//    }
//    
//    /**
//     * Matches <code>BCrypt</code> Password.
//     * 
//     * @param rawPassword The Raw Password to validate.
//     * @param encodedPassword The Encoded Password to validate.
//     * @return true if the Raw Password matches with the Encoded Password.
//     */
//    public static boolean matchesBCryptPassword(String rawPassword,
//            String encodedPassword) {
//        BCryptPasswordEncoder encoderBCrypt = new BCryptPasswordEncoder();
//
//        return encoderBCrypt.matches(rawPassword, encodedPassword);
//    }

    /* Overrides: */
}
