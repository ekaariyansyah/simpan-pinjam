# Simpan Pinjam

##### How To Run Application?
Run: <code>mvn spring-boot:run</code>

##### How To Debug Application?
<code>java -agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=localhost:8000 -jar target\simpanpinjam-0.0.1-SNAPSHOT.jar</code>

##### Initial Data
This app needs initial data in <code>data.sql</code>