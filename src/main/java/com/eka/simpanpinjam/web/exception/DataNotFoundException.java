/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Data Not Found Exception
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Apr 1, 2022
 * @since 1.0.0
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class DataNotFoundException extends RuntimeException {

    /* Constants: */
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 538633248330399294L;


    /* Constructors: */
    public DataNotFoundException () {
	super();
    }
    
    public DataNotFoundException (String message) {
	super(message);
    }
}
