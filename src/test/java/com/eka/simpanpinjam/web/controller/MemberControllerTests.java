/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.web.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

/**
 * Test for Member Controller
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 31, 2022
 * @since 1.0.0
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MemberControllerTests {
    
    /* Attributes: */
    @LocalServerPort
    private int port;

    /* Beans: */
    @Autowired
    private TestRestTemplate testRestTemplate;
    
    @Autowired
    private MemberController memberController;
    
    
    /* Test Cases: */
    @Test
    void contextLoads() {
	assertThat(memberController).isNotNull();
    }
    
    @Test
    public void getAllTestShouldReturnOk() {
	assertThat(this.testRestTemplate.getForObject("http://localhost:" + port + "/simpan-pinjam/member", 
		String.class)).contains("\"success\":true");
    }

}
