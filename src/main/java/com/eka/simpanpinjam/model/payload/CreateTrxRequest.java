/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.model.payload;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

import com.eka.simpanpinjam.model.common.SimpanPinjamConstants;
import com.eka.simpanpinjam.model.common.TrxTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.NotNull;

import lombok.Data;

/**
 * Create Transaction Request Model
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@Data
public class CreateTrxRequest implements Serializable {

    /* Constants: */
    /**
     * 
     */
    private static final long serialVersionUID = 7458479308547256098L;

    /* Attributes: */
    /**
     * Transaction Amount
     */
    @NotNull
    @Positive
    private BigDecimal amount;
    
    /**
     * Transaction Type
     */
    @NotBlank
    private String transactionType;
    
    /**
     * Transaction remarks
     */
    @NotBlank
    @Pattern(regexp = SimpanPinjamConstants.ALPHANUMERIC_SPACE_REGEX, message="alphanumeric only")
    private String remarks;
    
    /**
     * Member Id
     */
    @NotNull
    private Long memberId;
    
    @Past
    @JsonFormat(pattern = SimpanPinjamConstants.DATETIME_FORMAT)
    private LocalDateTime transactionDateTime;


    /* Constructors: */


    /* Functionalities: */

    /* Overrides: */
}
