/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.core.service;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.eka.simpanpinjam.core.repository.AccountBalanceRepository;
import com.eka.simpanpinjam.core.repository.AppUserRepository;
import com.eka.simpanpinjam.core.util.SimpanPinjamUtil;
import com.eka.simpanpinjam.model.common.CommonRequest;
import com.eka.simpanpinjam.model.common.CommonResponse;
import com.eka.simpanpinjam.model.common.Paging;
import com.eka.simpanpinjam.model.entity.AccountBalance;
import com.eka.simpanpinjam.model.entity.AppUser;
import com.eka.simpanpinjam.web.exception.DataNotFoundException;

/**
 * Application User Service
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@Service
public class AppUserService {
    /* Constants: */

    /* Attributes: */
    @Autowired
    private AppUserRepository appUserRepo;
    
    @Autowired
    private AccountBalanceRepository accountBalanceRepo;

    /* Transient Attributes: */

    /* Constructors: */

    /* Getters & setters for attributes: */

    /* Getters & setters for transient attributes: */

    /* Functionalities: */
    /**
     * Save User
     * @param appUser
     * @return
     */
    @Transactional
    public AppUser createUser(AppUser appUser) {
	appUser.setId(null);
	// add username already exist
	AppUser exist = appUserRepo.findByUsername(appUser.getUsername());
	if(exist!=null) {
	    throw new ValidationException("username already exist: "+appUser.getUsername());
	}
	
//	// encode pwd
//	String encodedPwd = SimpanPinjamUtil.encodeBCryptPassword(appUser.getPwd());
//	appUser.setPwd(encodedPwd);
	
	// save user
	appUser = appUserRepo.save(appUser);
	
	// save account balance
	AccountBalance accountBalance = new AccountBalance();
	accountBalance.setBalance(BigDecimal.ZERO);
	accountBalance.setMasterBalance(false);
	accountBalance.setMember(appUser);
	accountBalanceRepo.save(accountBalance);
	
	return appUser;
    }
    
    /**
     * Find All Users
     * @return
     */
    public List<AppUser> findAll() {
	return appUserRepo.findAll();
    }
    
    public CommonResponse retrieveUser(CommonRequest<AppUser> request) {
	CommonResponse response = CommonResponse.createSuccess();
	
	// TODO prepare query
	
	// call query
	Page<AppUser> pageMember = appUserRepo.findAll(request.getPaging().getPageable());
	
	// set data
	List<AppUser> members = pageMember.getContent();
	response.setData(members);
	
	// set paging response
	Paging paging = new Paging(pageMember.getNumber() + 1, pageMember.getNumberOfElements(),
		pageMember.getTotalPages(), pageMember.getTotalElements());
	response.setPaging(paging);
	
	return response;
    }
    
    /**
     * Find Member by Id
     * @param memberId Member Id to find
     * @return
     */
    public AppUser findById(Long memberId) {
	AppUser member = appUserRepo.findById(memberId).orElse(null);
	if(member==null) {
	    throw new DataNotFoundException("member not found: "+memberId);
	}
	return member;
    }
    
    /**
     * Update User
     * @param appUser
     * @return
     */
    public AppUser updateUser(AppUser appUser) {
	// validate request
	if(appUser.getId()==null) {
	  throw new ValidationException("id is required for update");  
	}
	
	// validate existance
	findById(appUser.getId());
	
	// avoid duplicate username
	AppUser exist = appUserRepo.findByUsername(appUser.getUsername());
	if(exist.getId().compareTo(appUser.getId())!=0) {
	    throw new ValidationException("username already exist");
	}
	
//	// encode pwd
//	String encodedPwd = SimpanPinjamUtil.encodeBCryptPassword(appUser.getPwd());
//	appUser.setPwd(encodedPwd);
	
	appUser = appUserRepo.save(appUser);
	
	return appUser;
    }
    
    /**
     * Delete User By MemberId
     * @param memberId
     */
    public void deleteUserById(Long memberId) {
	// validate if still has balance
	AppUser member = findById(memberId);
	AccountBalance balance = accountBalanceRepo.findByMemberId(memberId);
	if(balance==null) {
	    throw new DataNotFoundException("account balance not found");
	}
	if(balance.getBalance().compareTo(BigDecimal.ZERO)!=0) {
	    throw new ValidationException("Delete failed. User still has balance.");
	}
	
	// delete member
	appUserRepo.delete(member);
    }
    
    /**
     * Find By Username
     * @param username
     * @return
     */
    public AppUser findByUsername(String username) {
	AppUser member = appUserRepo.findByUsername(username);
	if(member==null) {
	    throw new DataNotFoundException("member not found: "+username);
	}
	
	return member;
    }
    

    /* Overrides: */
}
