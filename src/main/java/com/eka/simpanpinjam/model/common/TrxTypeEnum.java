/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.model.common;

/**
 * Transaction Type Enum
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 31, 2022
 * @since 1.0.0
 */
public enum TrxTypeEnum {
    SIMPAN,
    PINJAM
}
