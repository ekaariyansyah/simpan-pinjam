/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.model.common;

import java.io.Serializable;

import javax.validation.Valid;

import lombok.Data;

/**
 * Common Request Model
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@Data
public class CommonRequest<T> implements Serializable {
    
    /* Constants: */
    /**
     * Serial Version UID 
     */
    private static final long serialVersionUID = -6361486024383896715L;

    /* Attributes: */
    /**
     * Pagination Request
     */
    @Valid
    private Paging paging;
    
    /**
     * Parameter
     */
    private T parameter;


    /* Functionalities: */

}
