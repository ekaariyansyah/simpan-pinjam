/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.model.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.Positive;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.eka.simpanpinjam.model.common.SimpanPinjamConstants;
import com.eka.simpanpinjam.model.common.TrxTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

import lombok.Data;

/**
 * Transaction Entity Model
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "trx")
public class Transaction extends BaseEntity {
    /* Constants: */

    /**
     * 
     */
    private static final long serialVersionUID = 6748472003433794118L;

    /* Attributes: */
    /**
     * Transaction Identity
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    /**
     * Transaction amount. Positive value for CREDIT, negative value for DEBIT.
     */
    @Column(name = "amount")
    private BigDecimal amount;
    
    /**
     * Balance before transaction.
     */
    @Column(name = "current_balance")
    private BigDecimal currentBalance;
    
    /**
     * Transaction Remarks.
     */
    @Column(name = "remarks")
    private String remarks;
    
    /**
     * Member who did the transaction.
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "member", referencedColumnName = "id")
    private AppUser member;
    
    @Column(name = "trx_type")
    private TrxTypeEnum transactionType;
    
    /**
     * Transaction Date Time
     */
    @Past
    @JsonFormat(pattern = SimpanPinjamConstants.DATETIME_FORMAT)
    @Column(name = "trx_date_time")
    private LocalDateTime transactionDateTime;
    

    /* Transient Attributes: */
    @Transient
    private String memberName;


    /* Constructors: */

    /* Getters & setters for attributes: */

    /* Getters & setters for transient attributes: */
    /**
     * Gets <code>fullName</code>.
     * @return The <code>fullName</code>.
     */
    public String getMemberName() {
	if(member!=null) {
	    return member.getFullName();
	} else {
	    return memberName;
	}
    }
    

    /* Functionalities: */

    /* Overrides: */
}
