/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.core.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger Configuration
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Apr 1, 2022
 * @since 1.0.0
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    /* Constants: */
    private static final Set<String> DEFAULT_PRODUCES = 
	      new HashSet<String>(Arrays.asList("application/json"));
    private static final Set<String> DEFAULT_CONSUMES = 
	      new HashSet<String>(Arrays.asList("application/json"));

    /* Attributes: */

    /* Transient Attributes: */

    /* Constructors: */

    /* Getters & setters for attributes: */

    /* Getters & setters for transient attributes: */

    /* Functionalities: */
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("com.eka.simpanpinjam.web.controller"))              
          .paths(PathSelectors.any())                          
          .build()
          .consumes(DEFAULT_CONSUMES)
          .produces(DEFAULT_PRODUCES);                                          
    }

    /* Overrides: */
}
