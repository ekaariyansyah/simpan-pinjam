/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.web.controller;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.eka.simpanpinjam.core.service.AppUserService;
import com.eka.simpanpinjam.model.common.CommonRequest;
import com.eka.simpanpinjam.model.common.Paging;
import com.eka.simpanpinjam.model.common.TrxTypeEnum;
import com.eka.simpanpinjam.model.payload.CreateTrxRequest;
import com.eka.simpanpinjam.model.payload.RetrieveTrxRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Test Class for Transaction Controller
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Apr 1, 2022
 * @since 1.0.0
 */
@SpringBootTest
@AutoConfigureMockMvc
public class TransactionControllerTestsMock {
    /* Constants: */
    private final Long EXISTING_MEMBER_ID = 7L;
    private final String EXISTING_USERNAME = "master";
    private final Long EXISTING_MEMBER_ID2 = 8L;
    private final String EXISTING_USERNAME2 = "member";
    
    private final Long NOT_EXIST_MEMBER_ID = Long.valueOf(-1);
    
    private final BigDecimal AMOUNT = BigDecimal.valueOf(5_000_000);
    private final BigDecimal AMOUNT_BIG = BigDecimal.valueOf(1_000_000_000);
    
    private final String SAMPLE_REMARKS_SIMPAN = "TEST Simpan";
    private final String SAMPLE_REMARKS_PINJAM = "TEST Pinjam";
    private final LocalDateTime SAMPLE_DATETIME = LocalDate.of(2022, 01, 17).atStartOfDay();
    private final LocalDateTime SAMPLE_DATETIME_FUTURE = LocalDate.of(2023, 10, 7).atStartOfDay();
    
    private final LocalDate DATE_START = LocalDate.of(2020, 01, 01);
    private final LocalDate DATE_END = LocalDate.now();
    
    private ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
    

    /* Beans: */
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private AppUserService appUserService;
    
    

    /* ------ PUT Create Transaction ------ */
    /**
     * Create Transaction Test
     * @throws Exception
     */
    @Test
    public void createTrxShouldReturnOk() throws Exception {
	CreateTrxRequest request = constructTrxRequest();
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.put("/transaction")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	
	mockMvc.perform(mockReq).andExpect(status().isCreated())
		.andExpect(jsonPath("$.success", is(true)));
	
    }
    
    /**
     * Create Transaction with Date in Future
     * @throws Exception
     */
    @Test
    public void createTrxFuture() throws Exception {	
	CreateTrxRequest request = constructTrxRequest();
	request.setTransactionDateTime(SAMPLE_DATETIME_FUTURE);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.put("/transaction")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	
	mockMvc.perform(mockReq).andExpect(status().isBadRequest())
		.andExpect(jsonPath("$.success", is(false)))
		.andExpect(jsonPath("$.message", stringContainsInOrder("past date", "transactionDateTime")));
	
    }
    
    /**
     * Create Transaction with false Member Id
     * @throws Exception
     */
    @Test
    public void createTrxMemberNotExist() throws Exception {
	CreateTrxRequest request = constructTrxRequest();
	request.setMemberId(NOT_EXIST_MEMBER_ID);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.put("/transaction")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	
	mockMvc.perform(mockReq).andExpect(status().isNotFound())
        	.andExpect(jsonPath("$.success", is(false)))
        	.andExpect(jsonPath("$.message", stringContainsInOrder("not","found")));
    }
    
    /**
     * Create Transaction with Negative Amount
     * @throws Exception
     */
    @Test
    public void createTrxNegativeAmount() throws Exception {
	CreateTrxRequest request = constructTrxRequest();
	request.setAmount(AMOUNT.negate());
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.put("/transaction")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	
	mockMvc.perform(mockReq).andExpect(status().isBadRequest())
        	.andExpect(jsonPath("$.success", is(false)))
        	.andExpect(jsonPath("$.message", stringContainsInOrder("must be greater than 0", "amount")));
    }
    
    /**
     * Create Transaction Pinjam with Big Amount (insufficient balance)
     * @throws Exception
     */
    @Test
    public void createTrxPinjamBigAmount() throws Exception {
	CreateTrxRequest request = constructTrxRequest();
	request.setRemarks(SAMPLE_REMARKS_PINJAM);
	request.setTransactionType(TrxTypeEnum.PINJAM.name());
	request.setAmount(AMOUNT_BIG);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.put("/transaction")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	
	mockMvc.perform(mockReq).andExpect(status().isNotAcceptable())
        	.andExpect(jsonPath("$.success", is(false)))
        	.andExpect(jsonPath("$.message", stringContainsInOrder("insufficient balance")));
    }
    
    
    /* ------ POST Retrieve Transaction ------ */
    /**
     * Retrieve Transaction with Date Range
     * @throws Exception
     */
    @Test
    public void retrieveTrxDateRange() throws Exception {
	Paging paging = new Paging(1, 5);
	RetrieveTrxRequest param = new RetrieveTrxRequest();
	param.setStartDate(DATE_START);
	param.setEndDate(DATE_END);
	
	CommonRequest<RetrieveTrxRequest> request = new CommonRequest<RetrieveTrxRequest>();
	request.setPaging(paging);
	request.setParameter(param);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.post("/transaction")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	
	mockMvc.perform(mockReq).andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(true)))
		.andExpect(jsonPath("$.data").isNotEmpty());
    }
    
    /**
     * Retrieve Transaction with Reverse Date (start date is after end date)
     * @throws Exception
     */
    @Test
    public void retrieveTrxDateReverse() throws Exception {
	Paging paging = new Paging(1, 5);
	RetrieveTrxRequest param = new RetrieveTrxRequest();
	param.setStartDate(DATE_END);
	param.setEndDate(DATE_START);
	
	CommonRequest<RetrieveTrxRequest> request = new CommonRequest<RetrieveTrxRequest>();
	request.setPaging(paging);
	request.setParameter(param);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.post("/transaction")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	
	mockMvc.perform(mockReq).andExpect(status().isNotAcceptable())
		.andExpect(jsonPath("$.success", is(false)))
		.andExpect(jsonPath("$.data").doesNotExist());
    }
    
    /**
     * Retrieve By Member Id
     * @throws Exception
     */
    @Test
    public void retrieveByMemberShouldOk() throws Exception {
	Paging paging = new Paging(1, 5);
	RetrieveTrxRequest param = new RetrieveTrxRequest();
	param.setMemberId(EXISTING_MEMBER_ID2);
	
	CommonRequest<RetrieveTrxRequest> request = new CommonRequest<RetrieveTrxRequest>();
	request.setPaging(paging);
	request.setParameter(param);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.post("/transaction")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	
	mockMvc.perform(mockReq).andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(true)))
		.andExpect(jsonPath("$.data").isNotEmpty());
    }
    
    
    /* ------ GET Get Member Transaction ------ */
    /**
     * Get Transaction by Member Should Ok
     * @throws Exception
     */
    @Test
    public void getTrxByMemberShouldOk() throws Exception {
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.get("/transaction/member/"+EXISTING_MEMBER_ID2)
		.accept(MediaType.APPLICATION_JSON);
	mockMvc.perform(mockReq).andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(true)))
		.andExpect(jsonPath("$.data").isNotEmpty());
    }
    
    /**
     * Get Transaction by Member Not Found
     * @throws Exception
     */
    @Test
    public void getTrxByMemberNotFound() throws Exception {
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.get("/transaction/member/"+NOT_EXIST_MEMBER_ID)
		.accept(MediaType.APPLICATION_JSON);
	mockMvc.perform(mockReq).andExpect(status().isNotFound())
		.andExpect(jsonPath("$.success", is(false)))
		.andExpect(jsonPath("$.message", stringContainsInOrder("not","found")));
    }
    
    
    
    
    private CreateTrxRequest constructTrxRequest() {
	CreateTrxRequest request = new CreateTrxRequest(); 
	request.setAmount(AMOUNT);
	request.setMemberId(EXISTING_MEMBER_ID2);
	request.setRemarks(SAMPLE_REMARKS_SIMPAN);
	request.setTransactionDateTime(SAMPLE_DATETIME);
	request.setTransactionType(TrxTypeEnum.SIMPAN.name());
	
	return request;
    }
    
}
