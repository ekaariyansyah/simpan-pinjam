/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.model.common;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Application Constants
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
public class SimpanPinjamConstants {
    
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String TIME_FORMAT = "HH:mm:ss";
    public static final ZoneId ZONE_ID = ZoneId.of("Asia/Jakarta");
    
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
    public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern(DATETIME_FORMAT);
    
    
    /* Regex */
    public static final String ALPHABETIC_SPACE_REGEX = "[A-Za-z ]+";
    public static final String ALPHANUMERIC_REGEX = "[0-9A-Za-z]+";
    public static final String ALPHANUMERIC_SPACE_REGEX = "[0-9A-Za-z ]+";

}
