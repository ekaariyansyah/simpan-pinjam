package com.eka.simpanpinjam.web.controller;

import com.eka.simpanpinjam.core.service.ProyekService;
import com.eka.simpanpinjam.model.common.CommonResponse;
import com.eka.simpanpinjam.model.entity.Proyek;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/proyek")
public class ProyekController {
    @Autowired
    private ProyekService proyekService;

    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CommonResponse createProyek(@RequestBody @Valid Proyek request) {
        Proyek result = proyekService.createProyek(request);
        return CommonResponse.createSuccessWithData(result);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse getProyek() {
        List<Proyek> proyeks = proyekService.getAllProyek();
        return CommonResponse.createSuccessWithData(proyeks);
    }
}
