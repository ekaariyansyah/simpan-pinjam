package com.eka.simpanpinjam.core.service;

import com.eka.simpanpinjam.core.repository.ProyekRepository;
import com.eka.simpanpinjam.model.entity.Proyek;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service to manage Proyek
 *
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@Service
public class ProyekService {
    @Autowired
    private ProyekRepository proyekRepository;

    /**
     * Create Proyek
     * @param proyek The Proyek object to create.
     * @return
     */
    public Proyek createProyek(Proyek proyek) {
        if(proyek.getId()!=null) {
            proyek.setId(null);
        }

        return proyekRepository.save(proyek);
    }

    /**
     * Get All Proyek
     * @return
     */
    public List<Proyek> getAllProyek() {
        return proyekRepository.findAll();
    }
}
