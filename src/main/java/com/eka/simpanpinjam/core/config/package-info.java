/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
/**
 * Configurations
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Apr 1, 2022
 * @since 1.0.0
 */
package com.eka.simpanpinjam.core.config;