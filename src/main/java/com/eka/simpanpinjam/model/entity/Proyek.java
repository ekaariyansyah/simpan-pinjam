package com.eka.simpanpinjam.model.entity;

import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;


/**
 * Proyek Entity Model
 *
 * @author Eka Ariyansyah
 * @version $Revision$, May 31, 2022
 * @since 1.0.0
 */
@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "proyek")
public class Proyek {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nama_proyek")
    private String namaProyek;

    @Column(name = "nama_lembaga")
    private String namaLembaga;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "nilai_proyek")
    private BigDecimal nilaiProyek;
}
