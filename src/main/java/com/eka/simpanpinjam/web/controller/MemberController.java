/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

import com.eka.simpanpinjam.core.service.AppUserService;
import com.eka.simpanpinjam.model.common.CommonRequest;
import com.eka.simpanpinjam.model.common.CommonResponse;
import com.eka.simpanpinjam.model.entity.AppUser;

/**
 * REST Controller to manage Member / User
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@CrossOrigin
@RequestMapping("/member")
@RestController
public class MemberController {
    /* Constants: */

    /* Services: */
    @Autowired
    private AppUserService appUserService;

    /* Functionalities: */
    /**
     * Add/ Create a Member
     * @param request
     * @return
     */
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CommonResponse createMember(@RequestBody @Valid AppUser request) {
	AppUser member = appUserService.createUser(request);
	
	return CommonResponse.createSuccessWithData(member);
    }
    
    /**
     * Get All data member
     * @return
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse getAll() {
	List<AppUser> members = appUserService.findAll();
	
	return CommonResponse.createSuccessWithData(members);
    }
    
    /**
     * Retrieve Member with Pagination
     * @param request
     * @return
     */
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse retrieveMember(@RequestBody @Valid CommonRequest<AppUser> request) {
	CommonResponse response = appUserService.retrieveUser(request);
	
	return response;
    }
    
    /**
     * Get By Member Id
     * @param memberId Member Id to find
     * @return
     */
    @GetMapping("/{memberId}")
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse getById(@PathVariable("memberId") Long memberId) {
	return CommonResponse.createSuccessWithData(appUserService.findById(memberId));
    }
    
    /**
     * Update a Member
     * @param request
     * @return
     */
    @PatchMapping
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse updateMember(@RequestBody @Valid AppUser request) {
	AppUser member = appUserService.updateUser(request);
	
	return CommonResponse.createSuccessWithData(member);
    }
    
//    /**
//     * Delete Member
//     * @param memberId
//     * @return
//     */
//    @DeleteMapping("/{memberId}")
//    public CommonResponse deleteMember(@PathVariable("memberId") Long memberId) {
//	appUserService.deleteUserById(memberId);
//	
//	return CommonResponse.createSuccess();
//    }
    

    /* Overrides: */
}
