/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.core.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.eka.simpanpinjam.model.entity.Transaction;

/**
 * Transaction Repository
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    final String queryRetrieveTrx = "select t from Transaction t "
    	+ "where (cast(:startDate as date) is null or t.transactionDateTime between :startDate and :endDate) "
    	+ "and (:memberId is null or t.member.id = :memberId)";
    
    @Query(value = queryRetrieveTrx)
    Page<Transaction> findTransaction(
	    @Param("startDate") LocalDateTime startDate,
	    @Param("endDate") LocalDateTime endDate,
	    @Param("memberId") Long memberId,
	    Pageable pageable);
    
    List<Transaction> findByMemberId(Long memberId);
}
