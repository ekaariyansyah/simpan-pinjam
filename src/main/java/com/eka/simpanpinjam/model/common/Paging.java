/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.model.common;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * Pagination Response
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@Data
public class Paging {
    /* Constants: */

    /* Attributes: */
    @Min(1)
    private int page;
    
    @Min(1)
    @Max(100)
    private int size;
    
    private int totalPages;
    
    private long totalRecords;


    /* Constructors: */
    public Paging() {
	super();
    }
    
    
    public Paging(int page, int size) {
	this.page = page;
	this.size = size;
    }
    
    public Paging(int page, int size, int totalPages, long totalRecords) {
	this.page = page;
	this.size = size;
	this.totalPages = totalPages;
	this.totalRecords = totalRecords;
    }

    /* Functionalities: */
    @JsonIgnore
    public Pageable getPageable() {
	// pageable page start from 0 (page 1)
	int page = this.page > 0 ? this.page - 1 : this.page;
	return PageRequest.of(page, this.size);
    }

    /* Overrides: */
}
