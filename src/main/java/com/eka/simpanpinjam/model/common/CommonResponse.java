/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.model.common;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

/**
 * Common Response Model
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@Data
@JsonInclude(Include.NON_NULL)
public class CommonResponse implements Serializable {

    /* Constants: */
    /**
     * 
     */
    private static final long serialVersionUID = 8774204067790420516L;
    
    public static final String RESPONSE_MSG_SUCCESS = "Success";
    public static final String RESPONSE_MSG_FAILED = "Failed";
    public static final String RESPONSE_MSG_ERROR = "Ooopss.. Something went wrong. Please try again!";
    

    /* Attributes: */
//    /**
//     * Response Code
//     */
//    private String code;
    
    /**
     * Is request acceptable and success to get response.
     */
    private boolean success=true;
    
    /**
     * Response Message
     */
    private String message;
    
    /**
     * Response Data
     */
    private Object data;
    
    /**
     * Pagination info
     */
    private Paging paging;


    /* Constructors: */
    public CommonResponse() {
	super();
    }
    
    public CommonResponse(boolean success, String message) {
	super();
	this.success = success;
	this.message = message;
    }
    
    public CommonResponse(boolean success, String message, Object data) {
	super();
	this.success = success;
	this.message = message;
	this.data = data;
    }
    
    
    /* Functionalities: */ 
    public static CommonResponse createSuccess() {
	return new CommonResponse(true, RESPONSE_MSG_SUCCESS);
    }
    
    public static CommonResponse createSuccessWithData(Object data) {
	return new CommonResponse(true, RESPONSE_MSG_SUCCESS, data);
    }
    
    public static CommonResponse createError() {
	return new CommonResponse(false, RESPONSE_MSG_ERROR);
    }
}
