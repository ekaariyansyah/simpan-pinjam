/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.core.service;

import java.math.BigDecimal;

import javax.transaction.Transactional;
import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eka.simpanpinjam.core.repository.AccountBalanceRepository;
import com.eka.simpanpinjam.model.entity.AccountBalance;
import com.eka.simpanpinjam.model.entity.AppUser;
import com.eka.simpanpinjam.web.exception.DataNotFoundException;

/**
 * Service to Manage Account Balance
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@Service
public class AccountBalanceService {
    /* Constants: */
    private Logger log = LoggerFactory.getLogger(AccountBalanceService.class);

    /* Attributes: */
    @Autowired
    private AccountBalanceRepository accountBalanceRepo;

    /* Transient Attributes: */

    /* Constructors: */

    /* Getters & setters for attributes: */

    /* Getters & setters for transient attributes: */

    /* Functionalities: */    
    /**
     * Update Master Balance (Include check balance first)
     * @param amount  The Amount to be add into balance. It can be negative or positive.
     * @return the New Balance
     */
    @Transactional
    public AccountBalance updateMasterBalance(BigDecimal amount) {
	// get master balance
	AccountBalance account = accountBalanceRepo.findByMasterBalance(true);
	if(account==null) {
	    log.error("account balance not found");
	    throw new DataNotFoundException("Account Balance not found");
	} else {
	    // check balance
	    BigDecimal balance = account.getBalance().add(amount);
	    boolean isValid = balance.compareTo(BigDecimal.ZERO) >= 0;
	    if(isValid) {
		// update balance
		account.setBalance(balance);
		account = accountBalanceRepo.save(account);
		return account;
	    } else {
		log.error("Insufficient balance");
		throw new ValidationException("insufficient balance");
	    }
	}
    }
    
    /**
     * updateMemberBalance
     * @param member
     * @param amount The amount (in could be possitive or negative)
     * @return
     */
    @Transactional
    public AccountBalance updateMemberBalance(AppUser member, BigDecimal amount) {
	// find member account
	AccountBalance balance = accountBalanceRepo.findByMemberId(member.getId());
	if(balance==null) {
	    log.error("account balance not found");
	    throw new DataNotFoundException("Account Balance not found.");
	} else {
		// check balance
		BigDecimal balanceAmount = balance.getBalance().add(amount);
		boolean isValid = balanceAmount.compareTo(BigDecimal.ZERO) >= 0;
		if(isValid) {
//			// update balance
//			account.setBalance(balanceAmount);
//			account = accountBalanceRepo.save(account);
//			return account;
		} else {
			log.error("Insufficient balance");
			throw new ValidationException("insufficient balance");
		}
	}
	
	// update balance
	balance.setBalance(balance.getBalance().add(amount));
	return accountBalanceRepo.save(balance);
    }

    /* Overrides: */
}
