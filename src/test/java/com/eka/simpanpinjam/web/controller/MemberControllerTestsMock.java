/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.web.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;
//import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.eka.simpanpinjam.core.service.AppUserService;
import com.eka.simpanpinjam.model.common.CommonRequest;
import com.eka.simpanpinjam.model.common.Paging;
import com.eka.simpanpinjam.model.entity.AppUser;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Test Class for Member Controller
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 31, 2022
 * @since 1.0.0
 */
@SpringBootTest
@AutoConfigureMockMvc
public class MemberControllerTestsMock {
    /* Constants: */
    private final String SAMPLE_USERNAME = "membertest";
    private final String SAMPLE_PWD = "abc123";
    private final String SAMPLE_NAME = "Joe Foe";
    private final LocalDate SAMPLE_DOB = LocalDate.of(1993, 10, 7);
    private final String SAMPLE_ADDRESS = "Jl. Kenari No. 22";
    
    private final LocalDate SAMPLE_DOB_FUTURE = LocalDate.of(2023, 10, 7);
    private final String SAMPLE_NAME_MIN = "NG";
    private final String SAMPLE_NAME_SYMBOL = "Walter $moke";
    private final String SAMPLE_NAME_UPDATE = "Joe Foe Update";
    
    private final int EXISTING_MEMBER_ID = 7;
    private final String EXISTING_USERNAME = "master";
    
    private final int EXISTING_MEMBER_ID2 = 8;
    private final String EXISTING_USERNAME2 = "member";
    
    private final int NOT_EXIST_MEMBER_ID = -1;
    
    private ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();

    /* Beans: */
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private AppUserService appUserService;
    

    /* ------ GET get Member By Id ------ */
    /**
     * TEST: get with existing member id
     * @throws Exception
     */
    @Test
    public void getByIdShouldReturnOk() throws Exception {
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.get("/member/"+EXISTING_MEMBER_ID)
		.contentType(MediaType.APPLICATION_JSON);
	mockMvc.perform(mockReq).andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(true)))
		.andExpect(jsonPath("$.data.id",is(EXISTING_MEMBER_ID)));
    }
    
    /**
     * TEST: get with < 0 member id
     * @throws Exception
     */
    @Test
    public void getByIdNotFound() throws Exception {
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.get("/member/"+NOT_EXIST_MEMBER_ID)
		.contentType(MediaType.APPLICATION_JSON);
	mockMvc.perform(mockReq).andExpect(status().isNotFound())
		.andExpect(jsonPath("$.success", is(false)))
		.andExpect(jsonPath("$.message", stringContainsInOrder("not","found")));
    }
    
    
    /* ------ GET get all member ------ */
    /**
     * TEST: get all
     * @throws Exception
     */
    @Test
    public void getAllShouldReturnOk() throws Exception {
	this.mockMvc.perform(get("/member")).andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(true)));
    }
    
    
    /* ------ PUT Create Member ------ */
    @Test
    public void createMemberShouldReturnCreated() throws Exception {
	AppUser request = new AppUser();
	request.setUsername(SAMPLE_USERNAME);
	request.setPwd(SAMPLE_PWD);
	request.setFullName(SAMPLE_NAME);
	request.setDateOfBirth(SAMPLE_DOB);
	request.setAddress(SAMPLE_ADDRESS);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.put("/member")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	
	mockMvc.perform(mockReq).andExpect(status().isCreated())
		.andExpect(jsonPath("$.success", is(true)));
    }


    /**
     * TEST create with date of birth future
     * @throws Exception
     */
    @Test
    public void createMemberWithDateFuture() throws Exception {
	AppUser request = new AppUser();
	request.setUsername(SAMPLE_USERNAME);
	request.setPwd(SAMPLE_PWD);
	request.setFullName(SAMPLE_NAME);
	request.setDateOfBirth(SAMPLE_DOB_FUTURE);
	request.setAddress(SAMPLE_ADDRESS);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.put("/member")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	
	mockMvc.perform(mockReq).andExpect(status().isBadRequest())
		.andExpect(jsonPath("$.success", is(false)))
		.andExpect(jsonPath("$.message", stringContainsInOrder("past date", "dateOfBirth")));
    }
    
    /**
     * TEST create with name < 3
     * @throws Exception
     */
    @Test
    public void createMemberWithNameMin() throws Exception {
	AppUser request = new AppUser();
	request.setUsername(SAMPLE_USERNAME);
	request.setPwd(SAMPLE_PWD);
	request.setFullName(SAMPLE_NAME_MIN);
	request.setDateOfBirth(SAMPLE_DOB);
	request.setAddress(SAMPLE_ADDRESS);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.put("/member")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	
	mockMvc.perform(mockReq).andExpect(status().isBadRequest())
		.andExpect(jsonPath("$.success", is(false)))
		.andExpect(jsonPath("$.message", stringContainsInOrder("size must be","between", "fullName")));
    }
    
    /**
     * TEST create with name contain symbol
     * @throws Exception
     */
    @Test
    public void createMemberWithNameSymbol() throws Exception {
	AppUser request = new AppUser();
	request.setUsername(SAMPLE_USERNAME);
	request.setPwd(SAMPLE_PWD);
	request.setFullName(SAMPLE_NAME_SYMBOL);
	request.setDateOfBirth(SAMPLE_DOB);
	request.setAddress(SAMPLE_ADDRESS);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.put("/member")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	
	mockMvc.perform(mockReq).andExpect(status().isBadRequest())
		.andExpect(jsonPath("$.success", is(false)))
		.andExpect(jsonPath("$.message", stringContainsInOrder("only Alphabet and Space are allowed","fullName")));
    }
    
    
    /* ------ POST Retrieve Member ------ */
    /**
     * Retrieve Member Should Return Ok
     * @throws Exception
     */
    @Test
    public void retrieveMemberShouldReturnOk() throws Exception {
	CommonRequest<AppUser> request = new CommonRequest<AppUser>();
	Paging paging = new Paging(1,10);
	request.setPaging(paging);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.post("/member")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	mockMvc.perform(mockReq).andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(true)));
    }
    
    /**
     * Retrieve Member With Negative Page
     * @throws Exception
     */
    @Test
    public void retrieveMemberWithNegativePage() throws Exception {
	CommonRequest<AppUser> request = new CommonRequest<AppUser>();
	Paging paging = new Paging(-1,10);
	request.setPaging(paging);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.post("/member")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	mockMvc.perform(mockReq).andExpect(status().isBadRequest())
		.andExpect(jsonPath("$.success", is(false)))
		.andExpect(jsonPath("$.message", stringContainsInOrder("must be greater than or equal to 1")));
    }
    
    
    /* ------ PATCH Update Member ------ */
    /**
     * Update Member Should Return Ok
     * @throws Exception
     */
    @Test
    public void updateMemberShouldReturnOk() throws Exception {
	AppUser request = appUserService.findById(8L);
	request.setFullName(SAMPLE_NAME_UPDATE);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.patch("/member")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	mockMvc.perform(mockReq).andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(true)))
		.andExpect(jsonPath("$.data.fullName", stringContainsInOrder(SAMPLE_NAME_UPDATE)));
	
    }
    
    /**
     * Update Member Not Found
     * @throws Exception
     */
    @Test
    public void updateMemberNotFound() throws Exception {
	AppUser request = new AppUser();
	request.setId(Long.valueOf(NOT_EXIST_MEMBER_ID));
	request.setUsername(SAMPLE_USERNAME);
	request.setPwd(SAMPLE_PWD);
	request.setFullName(SAMPLE_NAME);
	request.setDateOfBirth(SAMPLE_DOB);
	request.setAddress(SAMPLE_ADDRESS);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.patch("/member")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	mockMvc.perform(mockReq)
		.andExpect(status().isNotFound())
		.andExpect(jsonPath("$.success", is(false)));
    }
    
    /**
     * Update With Existing Username
     * @throws Exception
     */
    @Test
    public void updateUsernameWithExistingUsername() throws Exception {
	AppUser request = appUserService.findById(Long.valueOf(EXISTING_MEMBER_ID2));
	request.setUsername(EXISTING_USERNAME);
	
	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.patch("/member")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(request));
	mockMvc.perform(mockReq)
		.andExpect(status().isNotAcceptable())
		.andExpect(jsonPath("$.success", is(false)));
    }
    
    
//    /* ------ DELETE Delete Member ------ */
//    /**
//     * Delete Member Should Return Ok
//     * @throws Exception
//     */
//    @Test
//    public void deleteMemberShouldReturnOk() throws Exception {
//	AppUser member = appUserService.findByUsername(SAMPLE_USERNAME);
//	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.delete("/member/"+member.getId())
//		.contentType(MediaType.APPLICATION_JSON);
//	mockMvc.perform(mockReq).andExpect(status().isOk())
//		.andExpect(jsonPath("$.success", is(true)));
//    }
//    
//    /**
//     * Delete Member By not exist memberId
//     * @throws Exception
//     */
//    @Test
//    public void deleteMemberNotFound() throws Exception {
//	MockHttpServletRequestBuilder mockReq = MockMvcRequestBuilders.delete("/member/"+NOT_EXIST_MEMBER_ID)
//		.contentType(MediaType.APPLICATION_JSON);
//	mockMvc.perform(mockReq).andExpect(status().isNotFound())
//		.andExpect(jsonPath("$.success", is(false)))
//		.andExpect(jsonPath("$.message", stringContainsInOrder("not","found")));
//    }
    

    /* Overrides: */
}
