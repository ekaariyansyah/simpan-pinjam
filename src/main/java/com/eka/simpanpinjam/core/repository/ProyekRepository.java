package com.eka.simpanpinjam.core.repository;

import com.eka.simpanpinjam.model.entity.Proyek;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Proyek Repository
 *
 * @author Eka Ariyansyah
 * @version $Revision$, May 31, 2022
 * @since 1.0.0
 */
@Repository
public interface ProyekRepository extends JpaRepository<Proyek, Long> {

}
