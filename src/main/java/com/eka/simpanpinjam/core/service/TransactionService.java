/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.core.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eka.simpanpinjam.core.repository.AppUserRepository;
import com.eka.simpanpinjam.core.repository.TransactionRepository;
import com.eka.simpanpinjam.model.common.CommonRequest;
import com.eka.simpanpinjam.model.common.CommonResponse;
import com.eka.simpanpinjam.model.common.Paging;
import com.eka.simpanpinjam.model.common.TrxTypeEnum;
import com.eka.simpanpinjam.model.entity.AccountBalance;
import com.eka.simpanpinjam.model.entity.AppUser;
import com.eka.simpanpinjam.model.entity.Transaction;
import com.eka.simpanpinjam.model.payload.CreateTrxRequest;
import com.eka.simpanpinjam.model.payload.RetrieveTrxRequest;
import com.eka.simpanpinjam.web.exception.DataNotFoundException;

/**
 * Transaction Service
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@Service
public class TransactionService {
    /* Constants: */
    private Logger log = LoggerFactory.getLogger(TransactionService.class);

    /* Repositories: */
    @Autowired
    private TransactionRepository trxRepo;
    
    @Autowired
    private AppUserRepository userRepo;
    
    
    /* Services: */
    @Autowired
    private AccountBalanceService balanceService;
    

    /* Functionalities: */
    public CommonResponse retrieveTrx(CommonRequest<RetrieveTrxRequest> request) {
	CommonResponse response = CommonResponse.createSuccess();
	
	LocalDateTime start = null;
	LocalDateTime end = null;
	
	// validate date
	LocalDate now = LocalDate.now();
	if(request.getParameter().getStartDate()==null && request.getParameter().getEndDate()!=null) {
	    log.error("date parameters are required");
	    throw new ValidationException("date parameters are required");
	}
	if(request.getParameter().getStartDate()!=null && request.getParameter().getEndDate()==null) {
	    log.error("date parameters are required");
	    throw new ValidationException("date parameters are required");
	}

	if (request.getParameter().getStartDate() != null && 
		request.getParameter().getEndDate() != null) {
	    if (request.getParameter().getStartDate().isAfter(now)
		    || request.getParameter().getEndDate().isAfter(now)) {
		log.error("date parameter cannot greater than today");
		throw new ValidationException("date parameter cannot greater than today");
	    }
	    if (request.getParameter().getStartDate().isAfter(request.getParameter().getEndDate())) {
		log.error("start date > end date");
		throw new ValidationException("start date cannot greater than end date");
	    }
	    
	    start = request.getParameter().getStartDate().atTime(LocalTime.MIN);
	    end = request.getParameter().getEndDate().atTime(LocalTime.MAX);
	}
	
	// call query
	Page<Transaction> pageTrx = trxRepo.findTransaction(start, end, request.getParameter().getMemberId(), request.getPaging().getPageable());
	
	// set data
	List<Transaction> trx = pageTrx.getContent();
	response.setData(trx);
	
	// set paging response
	Paging paging = new Paging(pageTrx.getNumber() + 1, pageTrx.getNumberOfElements(),
		pageTrx.getTotalPages(), pageTrx.getTotalElements());
	response.setPaging(paging);
	
	return response;
    }
    
    /**
     * Create Transactions (Include Update Balance)
     * @param trxRequest
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Transaction createTrx(CreateTrxRequest trxRequest) {
	// validate member
	AppUser member = userRepo.findById(trxRequest.getMemberId()).orElse(null);
	if(member==null) {
	    log.error("member not found: {}", trxRequest.getMemberId());
	    throw new DataNotFoundException("member not found: "+ trxRequest.getMemberId());
	}
	
	BigDecimal realAmount = trxRequest.getAmount();
	// validate trx type
	TrxTypeEnum trxType = null;
	if (TrxTypeEnum.SIMPAN.name().equalsIgnoreCase(trxRequest.getTransactionType())) {
	    trxType = TrxTypeEnum.SIMPAN;
	} else if (TrxTypeEnum.PINJAM.name().equalsIgnoreCase(trxRequest.getTransactionType())) {
	    trxType = TrxTypeEnum.PINJAM;
	    realAmount = realAmount.negate();
	} else {
	    log.error("invalid transactionType: ", trxRequest.getTransactionType());
	    throw new ValidationException("transactionType should be "+TrxTypeEnum.SIMPAN.name()+" or "+TrxTypeEnum.PINJAM.name());
	}
	
	// default trx date
	if(trxRequest.getTransactionDateTime()==null) {
	    trxRequest.setTransactionDateTime(LocalDateTime.now());
	}
	
//	// Update master balance
//	AccountBalance accountBalance = balanceService.updateMasterBalance(realAmount);
	
	// update member balance
		AccountBalance accountBalance = balanceService.updateMemberBalance(member, realAmount);
	
	// insert Transaction
	BigDecimal initialBalance = accountBalance.getBalance().subtract(realAmount);
	Transaction trx = new Transaction();
	trx.setAmount(realAmount);
	trx.setCurrentBalance(initialBalance);
	trx.setRemarks(trxRequest.getRemarks());
	trx.setTransactionType(trxType);
	trx.setTransactionDateTime(trxRequest.getTransactionDateTime());
	trx.setMember(member);
	trx = trxRepo.save(trx);
	
	return trx;
    }
    
    /**
     * Get Transaction by Member Id
     * @param memberId The Member Id to search
     * @return
     */
    public List<Transaction> getTrxMember(Long memberId) {
	if(!userRepo.existsById(memberId)) {
	    log.error("member not found: {}", memberId);
	    throw new DataNotFoundException("member not found: "+ memberId);
	}
	
	return trxRepo.findByMemberId(memberId);
    }

    /* Overrides: */
}
