/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.model.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.eka.simpanpinjam.model.common.SimpanPinjamConstants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * Application User Entity Model
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "app_user")
public class AppUser extends BaseEntity {
    /* Constants: */

    /**
     * 
     */
    private static final long serialVersionUID = 5966392183707347468L;

    /* Attributes: */
    /**
     * App User Identity
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    /**
     * Username
     */
    @NotBlank
    @Size(min = 3, max = 20)
    @Pattern(regexp = SimpanPinjamConstants.ALPHANUMERIC_REGEX,
	    message="alphanumeric only")
    @Column(name = "username", unique = true, nullable = false)
    private String username;
    
    /**
     * Password hashed
     */
    @Column(name = "pwd", nullable = false)
    private String pwd;
    
    /**
     * Member Name
     */
    @NotBlank
    @Size(min = 3, max = 50)
    @Pattern(regexp = SimpanPinjamConstants.ALPHABETIC_SPACE_REGEX, 
    	message="only Alphabet and Space are allowed")
    @Column(name = "full_name", nullable = false)
    private String fullName;
    
    /**
     * Date of birth in 'yyyy-MM-dd'
     */
    @Past
    @JsonFormat(pattern = SimpanPinjamConstants.DATE_FORMAT)
    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;
    
    /**
     * User Address
     */
    @Column(name = "address")
    private String address;
    
    
    /* Transient Attributes: */
    @Transient
    private BigDecimal balance;
    
    @JsonIgnore
    @OneToOne(mappedBy = "member", fetch = FetchType.LAZY)
    private AccountBalance accountBalance;


    /* Constructors: */

    /* Getters & setters for attributes: */

    /* Getters & setters for transient attributes: */
    /**
     * Gets <code>balance</code>.
     * @return The <code>balance</code>.
     */
    public BigDecimal getBalance() {
	if(accountBalance==null) {
	    return BigDecimal.ZERO;
	} else {
	    return accountBalance.getBalance();	    
	}
    }

    /* Functionalities: */

    /* Overrides: */
}
