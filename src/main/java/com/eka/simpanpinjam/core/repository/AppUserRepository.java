/*
 * $Id$
 * 
 * Copyright (c) 2022 Aero Systems Indonesia, PT.
 * All rights reserved.
 * 
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
/*
 * Copyright (c) 2022 Lufthansa Systems Indonesia, PT. All rights reserved.
 */
package com.eka.simpanpinjam.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eka.simpanpinjam.model.entity.AppUser;

/**
 * Application User Repository
 * 
 * @author Eka Ariyansyah
 * @version $Revision$, Mar 30, 2022
 * @since 1.0.0
 */
@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long> {
    AppUser findByUsername(String username);
    
    Page<AppUser> findAll(Pageable pageable);
    
}
